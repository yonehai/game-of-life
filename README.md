# Game Of Life

Game Of Life is a simple mobile simulator of the famous zero-player game.

## Features

- Initial board setting and monitoring of the cells' 'life'.
- Game boards saving and loading.
- Game parameters settings.
- Providing of the general game history and rules.

## OS Support

- Android 8.0 or higher.

## Development Environment

| Frameworks/Libraries | Version | Branch | Commit | License | Resource |
| ------ | ------ | ------ | ------ | ------ | ------ |
| [Qt](https://www.qt.io/) | 5.15.2 | - | - | LGPL | [Qt for Open Source](https://download.qt.io/archive/qt/5.15/) |
| [MobileNavigationLibrary](https://gitlab.com/vakoms/mobilenavigationlibrary) | - | [development](https://gitlab.com/vakoms/mobilenavigationlibrary/-/tree/development) | [1db0ddcf](https://gitlab.com/vakoms/mobilenavigationlibrary/-/commit/1db0ddcf190e985ee86b4c0ee8e9ef019c95f52f) | - | [MobileNavigationLibrary](https://gitlab.com/vakoms/mobilenavigationlibrary) |

## Game Of Life Building Instruction

Game Of Life can be built using Qt 5.15.2. It is also recommended to use Qt Creator 4.15.

> Note: deployment issues were encountered on building Android applications using Qt Creator 5.0 and higher. 

To build Game Of Life, proceed with the following steps:

### Android:

1. Open Game Of Life project in Qt Creator.
2. Click on the `Kit Selector`.
3. In `Kit` section, choose `Android Qt 5.15.2 Clang Multi-Abi`.
4. Configure ABIs:
   1. Go to `Projects -> Build & Run -> Android Qt 5.15.2 Clang Multi-Abi -> Build -> Build Steps`.
   2. Open `qmake -> ABIs`.
   3. Choose `armeabi-v7a` or `arm64-v8a` depending on your device architecture. 
4. In `Build` section, choose `Debug` or `Release` mode.
5. In `Run` section, choose the `GameOfLife` project.
7. Click `Build` to build or `Run` to run the application.
