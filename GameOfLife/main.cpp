#include <QQmlApplicationEngine>
#include <QGuiApplication>
#include <QQmlContext>

#include "navigationcontroller.h"
#include "navigationmanager.h"

#include "ui/controllers/menuscreencontroller.h"
#include "ui/appscreenfactory.h"
#include "core/gameboarddata.h"
#include "core/appmanager.h"
#include "ui/gameboard.h"
#include "ui/guiutils.h"

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    AppManager appManager;

    NavigationManager* navigationManager = new NavigationManager(&engine);
    navigationManager->addNavigationController(
                QStringLiteral("standard"),
                std::make_unique<NavigationController>(
                    std::make_unique<AppScreenFactory>(appManager)));
    navigationManager->registerTypes();
    engine.rootContext()->setContextProperty("AppNavigationManager", navigationManager);
    auto standardController = navigationManager->navigationController(QStringLiteral("standard"));

    GuiUtils::registerFonts();
    GameBoardData::initialize();
    GameBoard::initialize();

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    QObject::connect(&engine, &QQmlApplicationEngine::quit, &QGuiApplication::quit);

    standardController->push(AppScreenTypes::MenuScreen, QVariant(), 0);

    return app.exec();
}
