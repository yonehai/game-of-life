#include "gameplayer.h"

#include "gameboarddata.h"

GamePlayer::GamePlayer(GameBoardData &board, QObject *parent)
    : QObject(parent)
    , mBoard(board)
    , mStepsCount(0)
{
}

void GamePlayer::makeStep()
{
    QVector<QVector<bool>> mNewCellsGeneration = mBoard.cells();
    for (int row = 0; row < mBoard.cells().size(); ++row)
    {
        for (int column = 0; column < mBoard.cells().size(); ++column)
        {
            QVector<bool> neighbours = getNeighbours(row, column);
            int aliveNeighboursCount = std::count(neighbours.begin(), neighbours.end(), true);
            if (mBoard.cells().at(row).at(column) == true)
            {
                if (aliveNeighboursCount < 2 || aliveNeighboursCount > 3)
                {
                    mNewCellsGeneration[row][column] = false;
                }
            }
            else
            {
                if (aliveNeighboursCount == 3)
                {
                    mNewCellsGeneration[row][column] = true;
                }
            }
        }
    }
    if (mNewCellsGeneration.isEmpty() || mBoard.cells() == mNewCellsGeneration)
    {
        emit gameOver();
    }
    else
    {
        ++mStepsCount;
        emit stepsCountChanged();
    }
    mBoard.cells() = mNewCellsGeneration;
    emit mBoard.updated();
}

int GamePlayer::stepsCount() const
{
    return mStepsCount;
}

void GamePlayer::setStepsCount(int newStepsCount)
{
    mStepsCount = newStepsCount;
    emit stepsCountChanged();
}

QVector<bool> GamePlayer::getNeighbours(int row, int column)
{
    QVector<bool> neighbours;

    if (isOnBoard(row - 1, column - 1))
    {
        neighbours.push_back(mBoard.cells().at(row - 1).at(column - 1));
    }
    if (isOnBoard(row - 1, column))
    {
        neighbours.push_back(mBoard.cells().at(row - 1).at(column));
    }
    if (isOnBoard(row - 1, column + 1))
    {
        neighbours.push_back(mBoard.cells().at(row - 1).at(column + 1));
    }
    if (isOnBoard(row, column - 1))
    {
        neighbours.push_back(mBoard.cells().at(row).at(column - 1));
    }
    if (isOnBoard(row, column + 1))
    {
        neighbours.push_back(mBoard.cells().at(row).at(column + 1));
    }
    if (isOnBoard(row + 1, column - 1))
    {
        neighbours.push_back(mBoard.cells().at(row + 1).at(column - 1));
    }
    if (isOnBoard(row + 1, column))
    {
        neighbours.push_back(mBoard.cells().at(row + 1).at(column));
    }
    if (isOnBoard(row + 1, column + 1))
    {
        neighbours.push_back(mBoard.cells().at(row + 1).at(column + 1));
    }

    return neighbours;
}

bool GamePlayer::isOnBoard(int row, int column)
{
    return (row >= 0 && row < mBoard.cells().size())
            && (column >= 0 && column < mBoard.cells().size());
}
