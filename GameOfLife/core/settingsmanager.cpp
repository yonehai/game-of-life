#include "settingsmanager.h"

#include <QSettings>

SettingsManager::SettingsManager(QObject *parent)
    : QObject(parent)
    , mSettings(new QSettings(QSettings::IniFormat, QSettings::UserScope,
                              QStringLiteral("yonehai"), QStringLiteral("Game of Life")))
    , mBoardSize(mSettings->value(QStringLiteral("boardSize"), 12).toInt())
    , mStepDuration(mSettings->value(QStringLiteral("stepDuration"), 500).toInt())
    , mStepsForSuccessTimeout(mSettings->value(
                                  QStringLiteral("stepsForSuccessTimeout"), 20).toInt())
    , mHighscore(mSettings->value(QStringLiteral("highscore"), 0).toInt())
{
}

int SettingsManager::boardSize() const
{
    return mBoardSize;
}

void SettingsManager::setBoardSize(int newBoardSize)
{
    mSettings->setValue(QStringLiteral("boardSize"), newBoardSize);
    mBoardSize = newBoardSize;
    emit boardSizeChanged(mBoardSize);
}

int SettingsManager::stepDuration() const
{
    return mStepDuration;
}

void SettingsManager::setStepDuration(int newStepDuration)
{
    mSettings->setValue(QStringLiteral("stepDuration"), newStepDuration);
    mStepDuration = newStepDuration;
    emit stepDurationChanged(mStepDuration);
}

int SettingsManager::stepsForSuccessTimeout() const
{
    return mStepsForSuccessTimeout;
}

void SettingsManager::setStepsForSuccessTimeout(int newStepForSuccessTimeout)
{
    mSettings->setValue(QStringLiteral("stepsForSuccessTimeout"), newStepForSuccessTimeout);
    mStepsForSuccessTimeout = newStepForSuccessTimeout;
    emit stepsForSuccessTimeoutChanged(mStepsForSuccessTimeout);
}

int SettingsManager::highscore() const
{
    return mHighscore;
}

void SettingsManager::setHighscore(int newHighscore)
{
    mSettings->setValue(QStringLiteral("highscore"), newHighscore);
    mHighscore = newHighscore;
    emit highscoreChanged(mHighscore);
}
