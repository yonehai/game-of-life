#include "gameboarddata.h"

#include <QJsonObject>
#include <QJsonArray>
#include <QQmlEngine>

GameBoardData::GameBoardData(QObject *parent)
    : QObject(parent)
    , mRowCount(0)
    , mCells(QVector<QVector<bool>>(mRowCount, QVector<bool>(mRowCount, false)))
{
}

void GameBoardData::clear()
{
    mCells = QVector<QVector<bool>>(mRowCount, QVector<bool>(mRowCount, false));
    emit isEmptyChanged();
    emit updated();
}

bool GameBoardData::isEmpty()
{
    return std::find_if(mCells.begin(), mCells.end(), [](const QVector<bool> &row) {
        return std::find(row.begin(), row.end(), true) != row.end();
    }) == mCells.end();
}

int GameBoardData::id() const
{
    return mId;
}

void GameBoardData::setId(int newId)
{
    mId = newId;
}

int GameBoardData::rowCount() const
{
    return mRowCount;
}

void GameBoardData::setRowCount(int newRowCount)
{
    if (mRowCount != newRowCount)
    {
        mCells = QVector<QVector<bool>>(newRowCount, QVector<bool>(newRowCount, false));
        mRowCount = newRowCount;
        emit isEmptyChanged();
        emit updated();
    }
}

QString GameBoardData::boardName() const
{
    return mBoardName;
}

void GameBoardData::setBoardName(const QString &newBoardName)
{
    mBoardName = newBoardName;
}

QVector<QVector<bool>> &GameBoardData::cells()
{
    return mCells;
}

void GameBoardData::setCells(const QVector<QVector<bool>> &newCells)
{
    if (mCells != newCells)
    {
        mCells = newCells;
        emit isEmptyChanged();
        emit updated();
    }
}

void GameBoardData::fromJson(GameBoardData *gameBoardData, const QJsonObject &data)
{
    if (!data.isEmpty())
    {
        gameBoardData->setId(data.value(QStringLiteral("id")).toInt());
        gameBoardData->setBoardName(data.value(QStringLiteral("name")).toString());
        gameBoardData->setRowCount(data.value(QStringLiteral("rowCount")).toInt());
        gameBoardData->setCells(readCells(data.value(QStringLiteral("aliveCells")).toArray(),
                                          gameBoardData->rowCount()));
    }
    else
    {
        qDebug("ERROR: Couldn't read GameBoardData from empty QJsonObject");
    }
}

void GameBoardData::toJson(GameBoardData *gameBoardData, QJsonObject &data)
{
    data = QJsonObject({{QStringLiteral("id"), gameBoardData->id()},
                        {QStringLiteral("name"), gameBoardData->boardName()},
                        {QStringLiteral("rowCount"), gameBoardData->rowCount()},
                        {QStringLiteral("aliveCells"), writeCells(gameBoardData->cells())}});
}

void GameBoardData::initialize()
{
    qmlRegisterType<GameBoardData>("GameBoardData", 1, 0, "GameBoardData");
}

QVector<QVector<bool>> GameBoardData::readCells(const QJsonArray &data, int rowCount)
{
    QVector<QVector<bool>> cells(rowCount, QVector<bool>(rowCount, false));
    for (const QJsonValue &value : data)
    {
        int index = value.toInt();
        cells[index / rowCount][index % rowCount] = true;
    }
    return cells;
}

QJsonArray GameBoardData::writeCells(const QVector<QVector<bool>> &cells)
{
    QJsonArray data;
    int rowCount = cells.size();
    for (int row = 0; row < rowCount; ++row)
    {
        for (int column = 0; column < rowCount; ++column)
        {
            if (cells.at(row).at(column) == true)
            {
                data.append(row * rowCount + column);
            }
        }
    }
    return data;
}
