#include "savedboardsmanager.h"

#include <QStandardPaths>
#include <QJsonDocument>
#include <QJsonObject>
#include <QFile>
#include <QDir>

#include "gameboarddata.h"

SavedBoardsManager::SavedBoardsManager(QObject *parent)
    : QObject(parent)
    , mSavedBoardsFileDirectory(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation))
    , mSavedBoardsFilePath(mSavedBoardsFileDirectory + QStringLiteral("/savedBoards.json"))
    , mSavedBoardsFile(new QFile(mSavedBoardsFilePath, this))
{
    readBoards();
}

SavedBoardsManager::~SavedBoardsManager()
{
    writeBoards();
}

void SavedBoardsManager::readBoards()
{
    QDir directory(mSavedBoardsFileDirectory);
    if (!directory.exists())
    {
        directory.mkpath(mSavedBoardsFileDirectory);
    }
    if (mSavedBoardsFile->open(QIODevice::ReadWrite))
    {
        QJsonArray boards = QJsonDocument::fromJson(mSavedBoardsFile->readAll()).array();
        for (const QJsonValue &board : boards)
        {
            mBoards.push_back(new GameBoardData(this));
            GameBoardData::fromJson(mBoards.back(), board.toObject());
        }
        mSavedBoardsFile->close();
    }
    else
    {
        qDebug("ERROR: Couldn't open file %s", qPrintable(mSavedBoardsFilePath));
    }
}

void SavedBoardsManager::writeBoards()
{
    QJsonArray boardsData;
    for (GameBoardData *board : qAsConst(mBoards))
    {
        QJsonObject boardData;
        GameBoardData::toJson(board, boardData);
        boardsData.append(boardData);
    }
    if (mSavedBoardsFile->open(QIODevice::ReadWrite))
    {
        mSavedBoardsFile->write(QJsonDocument(boardsData).toJson());
        mSavedBoardsFile->close();
    }
    else
    {
        qDebug("ERROR: Couldn't open file %s", qPrintable(mSavedBoardsFilePath));
    }
}

QVector<GameBoardData*> &SavedBoardsManager::boards()
{
    return mBoards;
}

void SavedBoardsManager::addBoard(GameBoardData *gameBoardData)
{
    if (gameBoardData)
    {
        GameBoardData *newGameBoard = new GameBoardData(this);
        if (mBoards.isEmpty())
        {
            newGameBoard->setId(0);
        }
        else
        {
            newGameBoard->setId(mBoards.back()->id() + 1);
        }
        newGameBoard->setBoardName(gameBoardData->boardName());
        newGameBoard->setRowCount(gameBoardData->rowCount());
        newGameBoard->setCells(gameBoardData->cells());
        mBoards.push_back(newGameBoard);
    }
}

void SavedBoardsManager::removeBoard(int id)
{
    mBoards.erase(findBoard(id));
}

QVector<GameBoardData*>::iterator SavedBoardsManager::findBoard(int id)
{
    return std::find_if(mBoards.begin(), mBoards.end(),
                        [id](GameBoardData *data){ return data->id() == id; });
}
