#include "savedboardsmodel.h"

#include "savedboardsmanager.h"
#include "gameboarddata.h"

SavedBoardsModel::SavedBoardsModel(SavedBoardsManager &savedBoardsManager, QObject *parent)
    : QAbstractListModel(parent)
    , mSavedBoardsManager(savedBoardsManager)
{
}

int SavedBoardsModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return mSavedBoardsManager.boards().size();
}

QVariant SavedBoardsModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || (index.row() >= mSavedBoardsManager.boards().size()))
    {
        return QVariant();
    }

    GameBoardData* gameBoardData = mSavedBoardsManager.boards()[index.row()];
    if (!gameBoardData)
    {
        return QVariant();
    }

    switch (role)
    {
    case IdRole:
        return gameBoardData->id();
    case NameRole:
        return gameBoardData->boardName();
    case RowCountRole:
        return gameBoardData->rowCount();
    default:
        return QVariant();
    }
}

QHash<int, QByteArray> SavedBoardsModel::roleNames() const
{
    static QHash<int, QByteArray> names{
        {SavedBoardRoles::IdRole, QByteArrayLiteral("boardId")},
        {SavedBoardRoles::NameRole, QByteArrayLiteral("boardName")},
        {SavedBoardRoles::RowCountRole, QByteArrayLiteral("boardRowCount")}};
    return names;
}

void SavedBoardsModel::deleteBoard(int id)
{
    const auto position = mSavedBoardsManager.findBoard(id);
    const int rowIndex = std::distance(mSavedBoardsManager.boards().begin(), position);
    beginRemoveRows(QModelIndex(), rowIndex, rowIndex);
    mSavedBoardsManager.removeBoard(id);
    endRemoveRows();
}
