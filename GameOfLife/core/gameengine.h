#ifndef GAMEENGINE_H
#define GAMEENGINE_H

#include <QTimer>

class SettingsManager;
class GameBoardData;
class GamePlayer;

class GameEngine : public QObject
{
    Q_OBJECT
public:
    explicit GameEngine(SettingsManager &settingsManager, QObject *parent = nullptr);

    void start() const;
    void pause() const;
    void reset();

    GameBoardData *gameBoardData() const;
    void setGameBoardData(GameBoardData *newGameBoardData);

    int highscore() const;
    int stepsCount() const;
    int stepsForSuccessTimeout() const;

signals:
    void highscoreChanged();
    void stepsCountChanged();
    void gameOver(int stepsCount);

private:
    void makeStep();
    void stop();

    SettingsManager &mSettingsManager;
    GameBoardData *mGameBoardData;
    GamePlayer *mGamePlayer;
    QTimer *mTimer;
    int mStepsForSuccessTimeout;
};

#endif // GAMEENGINE_H
