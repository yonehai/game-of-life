#ifndef SETTINGSMANAGER_H
#define SETTINGSMANAGER_H

#include <QObject>

class QSettings;

class SettingsManager : public QObject
{
    Q_OBJECT
public:
    SettingsManager(QObject *parent = nullptr);

    int boardSize() const;
    void setBoardSize(int newBoardSize);

    int stepDuration() const;
    void setStepDuration(int newStepDuration);

    int stepsForSuccessTimeout() const;
    void setStepsForSuccessTimeout(int newStepForSuccessTimeout);

    int highscore() const;
    void setHighscore(int newHighscore);

signals:
    void boardSizeChanged(int boardSize);
    void stepDurationChanged(int stepDuration);
    void stepsForSuccessTimeoutChanged(int stepsForSuccessTimeout);
    void highscoreChanged(int highscore);

private:
    QSettings *mSettings;

    int mBoardSize;
    int mStepDuration;
    int mStepsForSuccessTimeout;
    int mHighscore;
};

#endif // SETTINGSMANAGER_H
