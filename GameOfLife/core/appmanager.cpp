#include "appmanager.h"

#include "savedboardsmanager.h"
#include "settingsmanager.h"
#include "gameengine.h"

AppManager::AppManager(QObject *parent)
    : QObject(parent)
    , mSettingsManager(new SettingsManager(this))
    , mSavedBoardsManager(new SavedBoardsManager(this))
    , mGameEngine(new GameEngine(*mSettingsManager, this))
{
}

SavedBoardsManager &AppManager::savedBoardsManager() const
{
    return *mSavedBoardsManager;
}

GameEngine &AppManager::gameEngine() const
{
    return *mGameEngine;
}

SettingsManager &AppManager::settingsManager() const
{
    return *mSettingsManager;
}
