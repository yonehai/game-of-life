#ifndef GAMEBOARDDATA_H
#define GAMEBOARDDATA_H

#include <QObject>

class GameBoardData : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool isEmpty READ isEmpty NOTIFY isEmptyChanged)
public:
    explicit GameBoardData(QObject *parent = nullptr);

    Q_INVOKABLE bool isEmpty();
    Q_INVOKABLE void clear();

    int id() const;
    void setId(int newId);

    int rowCount() const;
    void setRowCount(int newRowCount);

    QString boardName() const;
    void setBoardName(const QString &newBoardName);

    QVector<QVector<bool>> &cells();
    void setCells(const QVector<QVector<bool>> &newCells);

    static void fromJson(GameBoardData *gameBoardData, const QJsonObject &data);
    static void toJson(GameBoardData *gameBoardData, QJsonObject &data);

    static void initialize();

signals:
    void isEmptyChanged();
    void updated();

private:
    static QVector<QVector<bool>> readCells(const QJsonArray &data, int rowCount);
    static QJsonArray writeCells(const QVector<QVector<bool>> &cells);

    int mId;
    int mRowCount;
    QString mBoardName;
    QVector<QVector<bool>> mCells;
};

#endif // GAMEBOARDDATA_H
