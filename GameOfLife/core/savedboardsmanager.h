#ifndef SAVEDBOARDSMANAGER_H
#define SAVEDBOARDSMANAGER_H

#include <QJsonArray>
#include <QObject>

class QFile;

class GameBoardData;

class SavedBoardsManager : public QObject
{
    Q_OBJECT
public:
    SavedBoardsManager(QObject *parent = nullptr);
    ~SavedBoardsManager();

    QVector<GameBoardData*> &boards();

    void addBoard(GameBoardData *gameBoardData);
    void removeBoard(int id);
    QVector<GameBoardData*>::iterator findBoard(int id);

private:
    void readBoards();
    void writeBoards();

    const QString mSavedBoardsFileDirectory;
    const QString mSavedBoardsFilePath;
    QFile *mSavedBoardsFile;
    QVector<GameBoardData*> mBoards;
};

#endif // SAVEDBOARDSMANAGER_H
