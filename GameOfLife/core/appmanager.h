#ifndef APPMANAGER_H
#define APPMANAGER_H

#include <QObject>

class SavedBoardsManager;
class SettingsManager;
class GameEngine;

class AppManager : public QObject
{
    Q_OBJECT
public:
    AppManager(QObject *parent = nullptr);

    SavedBoardsManager &savedBoardsManager() const;
    GameEngine &gameEngine() const;
    SettingsManager &settingsManager() const;

private:
    SettingsManager *mSettingsManager;
    SavedBoardsManager *mSavedBoardsManager;
    GameEngine *mGameEngine;
};

#endif // APPMANAGER_H
