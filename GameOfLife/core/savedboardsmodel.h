#ifndef SAVEDBOARDSMODEL_H
#define SAVEDBOARDSMODEL_H

#include <QAbstractListModel>

class SavedBoardsManager;
class GameBoardData;

class SavedBoardsModel : public QAbstractListModel
{
public:
    enum SavedBoardRoles
    {
        IdRole = 0,
        NameRole,
        RowCountRole
    };

    explicit SavedBoardsModel(SavedBoardsManager &savedBoardsManager, QObject *parent = nullptr);

    int rowCount(const QModelIndex & parent = QModelIndex()) const override;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const override;
    QHash<int, QByteArray> roleNames() const override;

    void deleteBoard(int id);

private:
    SavedBoardsManager &mSavedBoardsManager;
};

#endif // SAVEDBOARDSMODEL_H
