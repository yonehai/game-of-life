#include "gameengine.h"

#include "settingsmanager.h"
#include "gameboarddata.h"
#include "gameplayer.h"

GameEngine::GameEngine(SettingsManager &settingsManager, QObject *parent)
    : QObject(parent)
    , mSettingsManager(settingsManager)
    , mGameBoardData(new GameBoardData(this))
    , mGamePlayer(new GamePlayer(*mGameBoardData, this))
    , mTimer(new QTimer(this))
    , mStepsForSuccessTimeout(settingsManager.stepsForSuccessTimeout())
{
    mGameBoardData->setRowCount(mSettingsManager.boardSize());
    mTimer->setInterval(mSettingsManager.stepDuration());

    connect(&mSettingsManager,
            &SettingsManager::boardSizeChanged,
            this,
            [this](int boardSize){ mGameBoardData->setRowCount(boardSize); });
    connect(&mSettingsManager,
            &SettingsManager::stepDurationChanged,
            this,
            [this](int stepDuration){ mTimer->setInterval(stepDuration); });
    connect(&mSettingsManager,
            &SettingsManager::stepsForSuccessTimeoutChanged,
            this,
            [this](int stepsForSuccessTimeout){ mStepsForSuccessTimeout = stepsForSuccessTimeout; });
    connect(&mSettingsManager,
            &SettingsManager::highscoreChanged,
            this,
            &GameEngine::highscoreChanged,
            Qt::UniqueConnection);
    connect(mGamePlayer,
            &GamePlayer::stepsCountChanged,
            this,
            &GameEngine::stepsCountChanged,
            Qt::UniqueConnection);
    connect(mGamePlayer, &GamePlayer::gameOver, this, &GameEngine::stop, Qt::UniqueConnection);
    connect(mTimer, &QTimer::timeout, this, &GameEngine::makeStep, Qt::UniqueConnection);
}

void GameEngine::start() const
{
    mTimer->start();
}

void GameEngine::pause() const
{
    mTimer->stop();
}

void GameEngine::reset()
{
    mGameBoardData->clear();
    mGamePlayer->setStepsCount(0);
    emit stepsCountChanged();
}

GameBoardData *GameEngine::gameBoardData() const
{
    return mGameBoardData;
}

void GameEngine::setGameBoardData(GameBoardData *newGameBoardData)
{
    reset();
    mGameBoardData->setId(newGameBoardData->id());
    mGameBoardData->setBoardName(newGameBoardData->boardName());
    mGameBoardData->setRowCount(newGameBoardData->rowCount());
    mGameBoardData->setCells(newGameBoardData->cells());
    mSettingsManager.setBoardSize(newGameBoardData->rowCount());
}

int GameEngine::highscore() const
{
    return mSettingsManager.highscore();
}

int GameEngine::stepsCount() const
{
    return mGamePlayer->stepsCount();
}

int GameEngine::stepsForSuccessTimeout() const
{
    return mStepsForSuccessTimeout;
}

void GameEngine::makeStep()
{
    mGamePlayer->makeStep();
}

void GameEngine::stop()
{
    mTimer->stop();
    const int stepsCount = mGamePlayer->stepsCount();
    if (stepsCount > mSettingsManager.highscore())
    {
        mSettingsManager.setHighscore(stepsCount);
    }
    emit gameOver(stepsCount);
}
