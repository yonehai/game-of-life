#ifndef GAMEPLAYER_H
#define GAMEPLAYER_H

#include <QObject>

class GameBoardData;

class GamePlayer : public QObject
{
    Q_OBJECT
public:
    GamePlayer(GameBoardData &board, QObject *parent = nullptr);

    void makeStep();

    int stepsCount() const;
    void setStepsCount(int newStepsCount);

signals:
    void stepsCountChanged();
    void gameOver();

private:
    QVector<bool> getNeighbours(int row, int column);
    bool isOnBoard(int row, int column);

    GameBoardData &mBoard;
    int mStepsCount;
};

#endif // GAMEPLAYER_H
