QT += quick svg

CONFIG += c++17

TARGET = GameOfLife

include(../mobilenavigationlibrary/MobileNavigationLibrary.pri)

HEADERS += \
    core/appmanager.h \
    core/gameboarddata.h \
    core/gameengine.h \
    core/gameplayer.h \
    core/savedboardsmanager.h \
    core/savedboardsmodel.h \
    core/settingsmanager.h \
    ui/appscreenfactory.h \
    ui/controllers/aboutscreencontroller.h \
    ui/controllers/gamescreencontroller.h \
    ui/controllers/menuscreencontroller.h \
    ui/controllers/savedscreencontroller.h \
    ui/controllers/settingsscreencontroller.h \
    ui/gameboard.h \
    ui/guiutils.h

SOURCES += \
        core/appmanager.cpp \
        core/gameboarddata.cpp \
        core/gameengine.cpp \
        core/gameplayer.cpp \
        core/savedboardsmanager.cpp \
        core/savedboardsmodel.cpp \
        core/settingsmanager.cpp \
        main.cpp \
        ui/appscreenfactory.cpp \
        ui/controllers/aboutscreencontroller.cpp \
        ui/controllers/gamescreencontroller.cpp \
        ui/controllers/menuscreencontroller.cpp \
        ui/controllers/savedscreencontroller.cpp \
        ui/controllers/settingsscreencontroller.cpp \
        ui/gameboard.cpp \
        ui/guiutils.cpp

RESOURCES += resources/qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
