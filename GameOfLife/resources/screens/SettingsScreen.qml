import QtQuick.Controls.Material 2.12
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick 2.15

import NavigationLibrary 1.0

import "../components"

BaseScreen {
    id: root

    property bool isSaved: boardSizeSlider.value === root.controller.boardSize
                           && stepDurationSlider.value === root.controller.stepDuration
                           && stepsForSuccessTimeoutSlider.value
                           === root.controller.stepsForSuccessTimeout

    ColumnLayout {
        anchors {
            fill: parent
            topMargin: 40
            bottomMargin: 32
            leftMargin: 20
            rightMargin: 20
        }
        spacing: 24

        Label {
            id: headerText
            Layout.preferredHeight: 36
            Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
            font {
                pixelSize: 36
                weight: Font.Medium
            }
            color: Material.accentColor
            text: qsTr("Settings")
        }

        ColumnLayout {
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignTop
            spacing: 8

            Label {
                Layout.fillWidth: true
                Layout.preferredHeight: 24
                Layout.alignment: Qt.AlignTop
                font.pixelSize: 24
                color: Material.accentColor
                text: qsTr("Board size")
            }

            ColumnLayout {
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignTop
                spacing: 0

                Slider {
                    id: boardSizeSlider
                    Layout.fillWidth: true
                    Layout.alignment: Qt.AlignTop
                    stepSize: 4
                    from: 12
                    to: 24
                    value: root.controller.boardSize
                }

                RowLayout {
                    Layout.fillWidth: true
                    Layout.alignment: Qt.AlignTop
                    spacing: 0

                    Label {
                        Layout.preferredHeight: 18
                        horizontalAlignment: Label.AlignHCenter
                        verticalAlignment: Label.AlignVCenter
                        font.pixelSize: 18
                        color: Material.accentColor
                        text: qsTr("12x12")
                    }

                    Item {
                        Layout.fillWidth: true
                    }

                    Label {
                        Layout.preferredHeight: 18
                        horizontalAlignment: Label.AlignHCenter
                        verticalAlignment: Label.AlignVCenter
                        font.pixelSize: 18
                        color: Material.accentColor
                        text: qsTr("16x16")
                    }

                    Item {
                        Layout.fillWidth: true
                    }

                    Label {
                        Layout.preferredHeight: 18
                        horizontalAlignment: Label.AlignHCenter
                        verticalAlignment: Label.AlignVCenter
                        font.pixelSize: 18
                        color: Material.accentColor
                        text: qsTr("20x20")
                    }

                    Item {
                        Layout.fillWidth: true
                    }

                    Label {
                        Layout.preferredHeight: 18
                        horizontalAlignment: Label.AlignHCenter
                        verticalAlignment: Label.AlignVCenter
                        font.pixelSize: 18
                        color: Material.accentColor
                        text: qsTr("24x24")
                    }
                }
            }
        }

        ColumnLayout {
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignTop
            spacing: 8

            Label {
                Layout.fillWidth: true
                Layout.preferredHeight: 24
                Layout.alignment: Qt.AlignTop
                font.pixelSize: 24
                color: Material.accentColor
                text: qsTr("Step duration")
            }

            ColumnLayout {
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignTop
                spacing: 0

                Slider {
                    id: stepDurationSlider
                    Layout.fillWidth: true
                    Layout.alignment: Qt.AlignTop
                    stepSize: 500
                    from: 500
                    to: 2000
                    value: root.controller.stepDuration
                }

                RowLayout {
                    Layout.fillWidth: true
                    Layout.alignment: Qt.AlignTop
                    spacing: 0

                    Label {
                        Layout.preferredHeight: 18
                        horizontalAlignment: Label.AlignHCenter
                        verticalAlignment: Label.AlignVCenter
                        font.pixelSize: 18
                        color: Material.accentColor
                        text: qsTr("0.5 s")
                    }

                    Item {
                        Layout.fillWidth: true
                    }

                    Label {
                        Layout.preferredHeight: 18
                        horizontalAlignment: Label.AlignHCenter
                        verticalAlignment: Label.AlignVCenter
                        font.pixelSize: 18
                        color: Material.accentColor
                        text: qsTr("1 s")
                    }

                    Item {
                        Layout.fillWidth: true
                    }

                    Label {
                        Layout.preferredHeight: 18
                        horizontalAlignment: Label.AlignHCenter
                        verticalAlignment: Label.AlignVCenter
                        font.pixelSize: 18
                        color: Material.accentColor
                        text: qsTr("1.5 s")
                    }

                    Item {
                        Layout.fillWidth: true
                    }

                    Label {
                        Layout.preferredHeight: 18
                        horizontalAlignment: Label.AlignHCenter
                        verticalAlignment: Label.AlignVCenter
                        font.pixelSize: 18
                        color: Material.accentColor
                        text: qsTr("2 s")
                    }
                }
            }
        }

        ColumnLayout {
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignTop
            spacing: 8

            Label {
                Layout.fillWidth: true
                Layout.preferredHeight: 24
                Layout.alignment: Qt.AlignTop
                font.pixelSize: 24
                color: Material.accentColor
                text: qsTr("Steps for success timeout")
            }

            ColumnLayout {
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignTop
                spacing: 0

                Slider {
                    id: stepsForSuccessTimeoutSlider
                    Layout.fillWidth: true
                    Layout.alignment: Qt.AlignTop
                    stepSize: 20
                    from: 20
                    to: 100
                    value: root.controller.stepsForSuccessTimeout
                }

                RowLayout {
                    Layout.fillWidth: true
                    Layout.alignment: Qt.AlignTop
                    spacing: 0

                    Label {
                        Layout.preferredHeight: 18
                        horizontalAlignment: Label.AlignHCenter
                        verticalAlignment: Label.AlignVCenter
                        font.pixelSize: 18
                        color: Material.accentColor
                        text: qsTr("20")
                    }

                    Item {
                        Layout.fillWidth: true
                    }

                    Label {
                        Layout.preferredHeight: 18
                        horizontalAlignment: Label.AlignHCenter
                        verticalAlignment: Label.AlignVCenter
                        font.pixelSize: 18
                        color: Material.accentColor
                        text: qsTr("40")
                    }

                    Item {
                        Layout.fillWidth: true
                    }

                    Label {
                        Layout.preferredHeight: 18
                        horizontalAlignment: Label.AlignHCenter
                        verticalAlignment: Label.AlignVCenter
                        font.pixelSize: 18
                        color: Material.accentColor
                        text: qsTr("60")
                    }

                    Item {
                        Layout.fillWidth: true
                    }

                    Label {
                        Layout.preferredHeight: 18
                        horizontalAlignment: Label.AlignHCenter
                        verticalAlignment: Label.AlignVCenter
                        font.pixelSize: 18
                        color: Material.accentColor
                        text: qsTr("80")
                    }

                    Item {
                        Layout.fillWidth: true
                    }

                    Label {
                        Layout.preferredHeight: 18
                        horizontalAlignment: Label.AlignHCenter
                        verticalAlignment: Label.AlignVCenter
                        font.pixelSize: 18
                        color: Material.accentColor
                        text: qsTr("100")
                    }
                }
            }
        }

        RowLayout {
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom
            spacing: 40

            PrimaryButton {
                id: backButton
                Layout.preferredWidth: 120
                text: qsTr("Back")

                onClicked: root.isSaved ? root.controller.pop() : quitConfirmationPopup.open()
            }

            PrimaryButton {
                id: saveButton

                Layout.preferredWidth: 120
                enabled: !root.isSaved
                text: qsTr("Save")

                onClicked: {
                    root.controller.boardSize = boardSizeSlider.value
                    root.controller.stepDuration = stepDurationSlider.value
                    root.controller.stepsForSuccessTimeout = stepsForSuccessTimeoutSlider.value
                }
            }
        }
    }

    ActionConfirmationPopup {
        id: quitConfirmationPopup
        x: (parent.width - width) / 2
        y: (parent.height - height) / 2
        headerText: qsTr("Do you really want to quit without saving?")
        leftButtonText: qsTr("Cancel")
        rightButtonText: qsTr("Quit")

        onLeftButtonClicked: close()

        onRightButtonClicked: {
            close()
            root.controller.pop()
        }
    }

    BackButton {
        anchors {
            top: parent.top
            left: parent.left
            topMargin: 14
            leftMargin: 14
        }

        onClicked: root.isSaved ? root.controller.pop() : quitConfirmationPopup.open()
    }
}
