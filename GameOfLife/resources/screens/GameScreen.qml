import QtQuick.Controls.Material 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick 2.15

import NavigationLibrary 1.0

import GameBoard 1.0

import "../components"

BaseScreen {
    id: root

    enum ButtonStates {
        Save,
        Start,
        Pause,
        Resume,
        Restart,
        Quit
    }

    ColumnLayout {
        anchors {
            fill: parent
            bottomMargin: 32
            topMargin: 40
            leftMargin: 20
            rightMargin: 20
        }
        spacing: 32

        RowLayout {
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter
            spacing: 20

            Label {
                Layout.preferredWidth: 120
                Layout.alignment: Qt.AlignLeft | Qt.AlignCenter
                font.pixelSize: 24
                color: Material.accentColor
                text: qsTr("Step: %1").arg(root.controller.stepsCount)
            }

            GameStopwatch {
                id: gameStopwatch
                Layout.preferredWidth: 120
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            }
        }

        GameBoard {
            id: gameBoard
            Layout.fillWidth: true
            Layout.preferredHeight: width
            gameBoardData: root.controller.gameBoardData()
        }

        RowLayout {
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom
            spacing: 40

            PrimaryButton {
                id: gameControlButton
                Layout.preferredWidth: 120
                enabled: !gameBoard.gameBoardData.isEmpty
                state: GameScreen.ButtonStates.Start
                states: [
                    State {
                        name: GameScreen.ButtonStates.Start

                        PropertyChanges {
                            target: gameControlButton
                            text: qsTr("Start")

                            onClicked: {
                                state = GameScreen.ButtonStates.Pause
                                saveButton.state = GameScreen.ButtonStates.Quit
                                gameBoard.enabled = false
                                root.controller.startGame()
                                gameStopwatch.startTimer()
                            }
                        }
                    },

                    State {
                        name: GameScreen.ButtonStates.Pause

                        PropertyChanges {
                            target: gameControlButton
                            text: qsTr("Pause")

                            onClicked: {
                                gameStopwatch.stopTimer()
                                state = GameScreen.ButtonStates.Resume
                                saveButton.state = GameScreen.ButtonStates.Quit
                                root.controller.pauseGame()
                            }
                        }
                    },

                    State {
                        name: GameScreen.ButtonStates.Resume

                        PropertyChanges {
                            target: gameControlButton
                            text: qsTr("Resume")

                            onClicked: {
                                state = GameScreen.ButtonStates.Pause
                                saveButton.state = GameScreen.ButtonStates.Quit
                                gameStopwatch.startTimer()
                                root.controller.startGame()
                            }
                        }
                    },

                    State {
                        name: GameScreen.ButtonStates.Restart

                        PropertyChanges {
                            target: gameControlButton
                            text: qsTr("Restart")

                            onClicked: {
                                root.controller.resetGame()
                                state = GameScreen.ButtonStates.Start
                                saveButton.state = GameScreen.ButtonStates.Save
                                gameStopwatch.resetTimer()
                                gameBoard.enabled = true
                            }
                        }
                    }
                ]
            }

            PrimaryButton {
                id: saveButton
                Layout.preferredWidth: 120
                enabled: !gameBoard.gameBoardData.isEmpty
                state: GameScreen.ButtonStates.Save
                states: [
                    State {
                        name: GameScreen.ButtonStates.Save

                        PropertyChanges {
                            target: saveButton
                            text: qsTr("Save")

                            onClicked: {
                                gameSavingPopup.open()
                            }
                        }
                    },

                    State {
                        name: GameScreen.ButtonStates.Quit

                        PropertyChanges {
                            target: saveButton
                            text: qsTr("Quit")

                            onClicked: {
                                root.controller.pauseGame()
                                gameStopwatch.stopTimer()
                                gameControlButton.state = GameScreen.ButtonStates.Resume
                                quitConfirmationPopup.open()
                            }
                        }
                    }
                ]
            }
        }
    }

    ActionConfirmationPopup {
        id: quitConfirmationPopup
        x: (parent.width - width) / 2
        y: (parent.height - height) / 2
        headerText: qsTr("Do you want to quit the game?")
        leftButtonText: qsTr("Cancel")
        rightButtonText: qsTr("Quit")

        onLeftButtonClicked: close()

        onRightButtonClicked: {
            root.controller.resetGame()
            close()
            root.controller.pop()
        }
    }

    GameSavingPopup {
        id: gameSavingPopup
        x: (parent.width - width) / 2
        y: (parent.height - height) / 2

        onSaveButtonClicked: {
            root.controller.saveBoard(boardName)
            close()
        }
    }

    GameOverPopup {
        id: gameOverPopup
        x: (parent.width - width) / 2
        y: (parent.height - height) / 2
        totalStepsCount: root.controller.stepsCount
        currentHighscore: root.controller.highscore
    }

    BackButton {
        anchors {
            top: parent.top
            left: parent.left
            topMargin: 14
            leftMargin: 14
        }

        onClicked: {
            root.controller.pauseGame()
            gameStopwatch.stopTimer()
            gameControlButton.state = GameScreen.ButtonStates.Resume
            quitConfirmationPopup.open()
        }
    }

    Connections {
        target: root.controller

        function onGameOver() {
            gameStopwatch.stopTimer()
            gameControlButton.state = GameScreen.ButtonStates.Restart
            gameOverPopup.open()
        }
    }
}
