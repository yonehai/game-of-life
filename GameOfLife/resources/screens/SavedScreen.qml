import QtQuick.Controls.Material 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick 2.15

import NavigationLibrary 1.0

import "../components"

BaseScreen {
    id: root

    property int selectedBoardId: -1

    enum ButtonStates {
        Delete,
        Cancel
    }

    ColumnLayout {
        anchors {
            fill: parent
            topMargin: 40
            bottomMargin: 32
        }
        spacing: 20

        Label {
            id: headerText
            Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
            font {
                pixelSize: 36
                weight: Font.Medium
            }
            color: Material.accentColor
            text: qsTr("Saved boards")
        }

        ListView {
            id: boardsView
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.leftMargin: 16
            Layout.rightMargin: 8
            clip: true
            delegate: RadioButton {
                width: boardsView.width
                leftPadding: 10
                rightPadding: 18
                spacing: 20
                font.pixelSize: 24
                Material.foreground: Material.accentColor
                ButtonGroup.group: boardsButtonGroup
                text: model.boardName
                onCheckedChanged: {
                    if (checked) {
                        root.selectedBoardId = model.boardId
                    }
                }
            }
            ScrollBar.vertical: AppScrollBar {}
            visible: model.length !== 0
            reuseItems: true
            model: root.controller.savedBoardsModel()

            ButtonGroup {
                id: boardsButtonGroup

                onCheckStateChanged: deleteButton.state = checkState === Qt.Unchecked
                                     ? SavedScreen.ButtonStates.Cancel
                                     : SavedScreen.ButtonStates.Delete
            }
        }

        ColumnLayout {
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom
            spacing: 32

            Label {
                id: boardName
                Layout.fillWidth: true
                Layout.preferredHeight: 24
                Layout.leftMargin: 26
                Layout.rightMargin: 26
                horizontalAlignment: Label.AlignHCenter
                verticalAlignment: Label.AlignVCenter
                font.pixelSize: 24
                color: Material.accentColor
                elide: Label.ElideRight
                text: {
                    if (boardsView.model.length === 0) {
                        return qsTr("No saved boards")
                    } else if ((boardsButtonGroup.checkState === Qt.Unchecked)
                               || (boardsButtonGroup.checkedButton == null)) {
                        return qsTr("Select the board")
                    } else {
                        return boardsButtonGroup.checkedButton.text
                    }
                }
            }

            RowLayout {
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignHCenter
                spacing: 40

                PrimaryButton {
                    id: deleteButton
                    Layout.preferredWidth: 120
                    state: SavedScreen.ButtonStates.Cancel
                    states: [
                        State {
                            name: SavedScreen.ButtonStates.Cancel

                            PropertyChanges {
                                target: deleteButton
                                text: qsTr("Back")

                                onClicked: root.controller.pop()
                            }
                        },

                        State {
                            name: SavedScreen.ButtonStates.Delete

                            PropertyChanges {
                                target: deleteButton
                                text: qsTr("Delete")

                                onClicked: boardDeletingConfirmationPopup.open()
                            }
                        }
                    ]
                }

                PrimaryButton {
                    id: startButton
                    Layout.preferredWidth: 120
                    enabled: boardsButtonGroup.checkState !== Qt.Unchecked
                    text: qsTr("Start")

                    onClicked: root.controller.loadBoard(root.selectedBoardId)
                }
            }
        }
    }

    ActionConfirmationPopup {
        id: boardDeletingConfirmationPopup
        x: (parent.width - width) / 2
        y: (parent.height - height) / 2
        headerText: qsTr("Do you really want to delete this board?")
        leftButtonText: qsTr("Cancel")
        rightButtonText: qsTr("Delete")

        onLeftButtonClicked: close()

        onRightButtonClicked: {
            root.controller.deleteBoard(root.selectedBoardId)
            boardsButtonGroup.checkState = Qt.Unchecked
            close()
        }
    }

    BackButton {
        anchors {
            top: parent.top
            left: parent.left
            topMargin: 14
            leftMargin: 14
        }

        onClicked: root.controller.pop()
    }
}
