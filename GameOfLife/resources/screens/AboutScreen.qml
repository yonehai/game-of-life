import QtQuick.Controls.Material 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick 2.15

import NavigationLibrary 1.0

import "../components"

BaseScreen {
    id: root

    ColumnLayout {
        anchors {
            fill: parent
            topMargin: 40
            bottomMargin: 20
        }
        spacing: 20

        Label {
            id: headerText
            Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
            font {
                pixelSize: 36
                weight: Font.Medium
            }
            color: Material.accentColor
            text: qsTr("About")
        }

        Flickable {
            Layout.fillHeight: true
            Layout.fillWidth: true
            contentHeight: aboutText.height
            clip: true
            Layout.leftMargin: 8
            Layout.rightMargin: 8
            ScrollBar.vertical: AppScrollBar {}

            Item {
                implicitWidth: parent.width - 24
                anchors.horizontalCenter: parent.horizontalCenter

                Label {
                    id: aboutText
                    width: parent.width
                    wrapMode: Label.WordWrap
                    text: root.controller.aboutGameText()
                    font.pixelSize: 18
                    color: Material.accentColor
                }
            }
        }
    }

    BackButton {
        anchors {
            top: parent.top
            left: parent.left
            topMargin: 14
            leftMargin: 14
        }

        onClicked: root.controller.pop()
    }
}
