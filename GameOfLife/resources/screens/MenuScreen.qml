import QtQuick.Controls.Material 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick 2.15

import NavigationLibrary 1.0

import "../components"

BaseScreen {
    id: root

    ColumnLayout {
        anchors {
            fill: parent
            margins: 40
        }
        spacing: 32

        Label {
            id: headerText
            Layout.alignment: Qt.AlignHCenter
            font {
                pixelSize: 36
                weight: Font.Medium
            }
            color: Material.accentColor
            text: qsTr("Game of Life")
        }

        ColumnLayout {
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.alignment: Qt.AlignHCenter
            spacing: 30

            PrimaryButton {
                id: startButton
                text: qsTr("Start")

                onClicked: root.controller.startButtonClicked()
            }

            PrimaryButton {
                id: savedBoardsButton
                text: qsTr("Saved boards")

                onClicked: root.controller.savedButtonClicked()
            }

            PrimaryButton {
                id: settingsButton
                text: qsTr("Settings")

                onClicked: root.controller.settingsButtonClicked()
            }

            PrimaryButton {
                id: aboutButton
                text: qsTr("About")

                onClicked: root.controller.aboutButtonClicked()
            }

            PrimaryButton {
                id: quitButton
                text: qsTr("Quit")

                onClicked: Qt.quit()
            }
        }

        Label {
            id: versionText
            Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom
            font.pixelSize: 24
            color: Material.accentColor
            text: qsTr("v0.1.0")
        }
    }
}
