import QtQuick.Controls.Material 2.15
import QtQuick.Controls 2.15
import QtQuick 2.15

TextField {
    id: root
    implicitWidth: 280
    implicitHeight: 50
    topPadding: 0
    bottomPadding: 0
    leftPadding: 12
    rightPadding: 12
    font.pixelSize: 24
    background: Rectangle {
        implicitWidth: 280
        implicitHeight: 50
        radius: 12
        color: Material.accentColor
    }
    cursorDelegate: Rectangle {
        implicitWidth: 1
        implicitHeight: parent.height - 24
        color: Material.primaryColor
    }
    selectionColor: Material.backgroundColor
    selectedTextColor: Material.accentColor
    placeholderTextColor: Qt.darker(Material.accent, 1.8)
}
