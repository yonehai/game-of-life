import QtQuick.Controls.Material 2.15
import QtQuick.Controls 2.15
import QtQuick 2.15

ScrollBar {
    id: root
    padding: 0
    contentItem: Rectangle {
        radius: 4
        color: Material.accent
    }
    background: Rectangle {
        implicitWidth: 4
        color: Qt.darker(Material.accent, 1.8)
    }
}
