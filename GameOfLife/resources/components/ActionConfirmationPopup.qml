import QtQuick.Controls.Material 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick 2.15

Popup {
    id: root

    property alias leftButtonText: leftButton.text
    property alias rightButtonText: rightButton.text
    property alias headerText: header.text

    signal leftButtonClicked()
    signal rightButtonClicked()

    implicitWidth: 320
    implicitHeight: 214
    padding: 0
    background: Rectangle {
        border {
            width: 2
            color: Material.accentColor
        }
        color: Material.backgroundColor
    }
    modal: true
    Overlay.modal: Rectangle {
        // INFO: Semi-transparent black
        color: "#7F000000"
    }
    closePolicy: Popup.NoAutoClose

    ColumnLayout {
        anchors {
            fill: parent
            topMargin: 32
            bottomMargin: 32
            leftMargin: 30
            rightMargin: 30
        }
        spacing: 32

        Label {
            id: header
            Layout.fillWidth: true
            Layout.preferredHeight: 58
            horizontalAlignment: Label.AlignHCenter
            verticalAlignment: Label.AlignVCenter
            font.pixelSize: 24
            color: Material.accentColor
            wrapMode: Label.WordWrap
        }

        RowLayout {
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter
            spacing: 20

            PrimaryButton {
                id: leftButton
                Layout.preferredWidth: 120

                onClicked: root.leftButtonClicked()
            }

            PrimaryButton {
                id: rightButton
                Layout.preferredWidth: 120

                onClicked: root.rightButtonClicked()
            }
        }
    }
}
