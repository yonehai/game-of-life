import QtQuick.Controls.Material 2.15
import QtQuick.Controls 2.15
import QtQuick 2.15

RoundButton {
    id: root
    implicitWidth: 240
    implicitHeight: 60
    padding: 0
    bottomInset: 0
    topInset: 0
    leftInset: 0
    rightInset: 0
    radius: 12
    Material.background: Material.accentColor
    font {
        pixelSize: 24
        weight: Font.Normal
        capitalization: Font.MixedCase
    }
}
