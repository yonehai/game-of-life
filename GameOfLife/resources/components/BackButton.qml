import QtQuick.Controls.Material 2.15
import QtQuick.Controls 2.15
import QtQuick 2.15

RoundButton {
    id: root
    padding: 0
    topInset: 0
    bottomInset: 0
    leftInset: 0
    rightInset: 0
    Material.elevation: 0
    Material.background: Material.backgroundColor
    icon {
        width: 32
        height: 32
        color: Material.accentColor
        source: "qrc:/images/backIcon.svg"
    }
}
