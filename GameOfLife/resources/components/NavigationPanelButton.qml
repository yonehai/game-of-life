import QtQuick.Controls 2.15
import QtQuick 2.15

TabButton {
    id: root

    property alias iconSource: buttonIcon.source

    implicitWidth: 120
    implicitHeight: 64
    contentItem: Item {
        Image {
            id: buttonIcon
            anchors.centerIn: parent
            sourceSize {
                width: 32
                height: 32
            }
        }
    }
}
