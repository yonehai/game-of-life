import QtQuick.Controls.Material 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick 2.15

Popup {
    id: root

    signal saveButtonClicked(string boardName)

    implicitWidth: 320
    implicitHeight: 214
    padding: 0
    background: Rectangle {
        border {
            width: 2
            color: Material.accentColor
        }
        color: Material.backgroundColor
    }
    modal: true
    Overlay.modal: Rectangle {
        // INFO: Semi-transparent black
        color: "#7F000000"
    }
    closePolicy: Popup.NoAutoClose

    ColumnLayout {
        anchors {
            fill: parent
            margins: 20
        }
        spacing: 20

        Label {
            Layout.fillWidth: true
            Layout.preferredHeight: 24
            horizontalAlignment: Label.AlignHCenter
            verticalAlignment: Label.AlignVCenter
            font.pixelSize: 24
            color: Material.accentColor
            text: qsTr("Save the board")
        }

        AppTextField {
            id: boardNameField
            Layout.fillWidth: true
            focus: true
            placeholderText: qsTr("Enter the board name")
        }

        RowLayout {
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter
            spacing: 20

            PrimaryButton {
                id: cancelButton
                Layout.preferredWidth: 120
                text: qsTr("Cancel")

                onClicked: root.close()
            }

            PrimaryButton {
                id: saveButton
                Layout.preferredWidth: 120
                enabled: boardNameField.text.length !== 0
                text: qsTr("Save")

                onClicked: root.saveButtonClicked(boardNameField.text)
            }
        }
    }

    onClosed: boardNameField.clear()
}
