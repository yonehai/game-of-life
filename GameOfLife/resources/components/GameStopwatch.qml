import QtQuick.Controls.Material 2.15
import QtQuick.Controls 2.15
import QtQuick 2.15

Rectangle {
    id: root

    function startTimer() {
        timer.start()
    }

    function stopTimer() {
        timer.stop()
    }

    function resetTimer() {
        timer.stop()
        timer.totalSeconds = 0
        stopwatchLabel.text = stopwatchLabel.initialText
    }

    implicitWidth: 120
    implicitHeight: implicitWidth / 2
    border {
        width: 4
        color: Material.accentColor
    }
    color: "transparent"

    Label {
        id: stopwatchLabel

        property string initialText: "00:00"

        anchors.centerIn: parent
        width: 70
        font.pixelSize: 24
        color: Material.accentColor
        text: initialText
    }

    Timer {
        id: timer

        property int totalSeconds: 0

        interval: 1000
        running: false
        repeat: true
        onTriggered: {
            ++totalSeconds

            const seconds = Math.floor(totalSeconds % 60).toString()
            const paddingSec = seconds.length < 2 ? "0" : ""

            const minutes = Math.floor(totalSeconds / 60).toString()
            const paddingMin = minutes.length < 2 ? "0" : ""

            stopwatchLabel.text = `${paddingMin}${minutes}:${paddingSec}${seconds}`
        }
    }
}
