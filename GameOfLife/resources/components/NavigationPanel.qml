import QtQuick.Controls.Material 2.15
import QtQuick.Controls 2.15
import QtQuick 2.15

TabBar {
    id: root

    signal savedButtonClicked()
    signal homeButtonClicked()
    signal settingsButtonClicked()

    implicitWidth: 360
    implicitHeight: 64
    spacing: 0
    Material.background: Material.primaryColor

    NavigationPanelButton {
        id: savedButton
        iconSource: "qrc:/images/bookmarkIcon.svg"

        onClicked: root.savedButtonClicked()
    }

    NavigationPanelButton {
        id: homeButton
        iconSource: "qrc:/images/homeIcon.svg"

        onClicked: root.homeButtonClicked()
    }

    NavigationPanelButton {
        id: settingsButton
        iconSource: "qrc:/images/settingsIcon.svg"

        onClicked: root.settingsButtonClicked()
    }

    Component.onCompleted: contentItem.highlight = null
}
