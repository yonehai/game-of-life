import QtQuick.Controls.Material 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick 2.15

Popup {
    id: root

    property int totalStepsCount: 0
    property int currentHighscore: 0

    implicitWidth: 320
    implicitHeight: 214
    padding: 0
    background: Rectangle {
        border {
            width: 2
            color: Material.accentColor
        }
        color: Material.backgroundColor
    }
    modal: true
    Overlay.modal: Rectangle {
        // INFO: Semi-transparent black
        color: "#7F000000"
    }
    closePolicy: Popup.NoAutoClose

    ColumnLayout {
        anchors {
            fill: parent
            margins: 20
        }
        spacing: 20

        Label {
            id: header
            Layout.fillWidth: true
            Layout.preferredHeight: 24
            horizontalAlignment: Label.AlignHCenter
            verticalAlignment: Label.AlignVCenter
            font {
                pixelSize: 24
                weight: Font.Medium
            }
            color: Material.accentColor
            wrapMode: Label.WordWrap
            text: qsTr("Game over!")
        }

        ColumnLayout {
            Layout.fillWidth: true
            Layout.fillHeight: true
            spacing: 12

            Label {
                id: totalSteps
                Layout.fillWidth: true
                Layout.preferredHeight: 24
                horizontalAlignment: Label.AlignHCenter
                verticalAlignment: Label.AlignVCenter
                font.pixelSize: 24
                color: Material.accentColor
                wrapMode: Label.WordWrap
                text: qsTr("Total steps: %1").arg(root.totalStepsCount)
            }

            Label {
                id: resultText
                Layout.fillWidth: true
                Layout.preferredHeight: 14
                horizontalAlignment: Label.AlignHCenter
                verticalAlignment: Label.AlignVCenter
                color: Material.accentColor
                wrapMode: Label.WordWrap
                text: root.totalStepsCount > root.currentHighscore
                      ? qsTr("New highscore!")
                      : qsTr("Current highscore: %1").arg(root.currentHighscore)
            }
        }

        PrimaryButton {
            id: okButton
            Layout.preferredWidth: 120
            Layout.alignment: Qt.AlignHCenter
            text: qsTr("OK")

            onClicked: root.close()
        }
    }
}
