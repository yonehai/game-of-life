import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick 2.15

import NavigationLibrary 1.0
import AppScreenTypes 1.0

import "components"

ApplicationWindow {
    id: root

    width: 360
    height: 640
    visible: true
    title: qsTr("Game of Life")

    ScreenView {
        id: screenView
        anchors.fill: parent
        navigationController: AppNavigationManager.navigationController("standard")
    }

    footer: NavigationPanel {
        visible: screenView.navigationController.currentScreen !== AppScreenTypes.MenuScreen
                 && screenView.navigationController.currentScreen !== AppScreenTypes.AboutScreen

        onHomeButtonClicked: {
            screenView.navigationController.popToScreen(AppScreenTypes.MenuScreen)
        }

        onSavedButtonClicked: {
            if (screenView.navigationController.contains(AppScreenTypes.SavedScreen)) {
                screenView.navigationController.popToScreen(AppScreenTypes.SavedScreen)
            } else {
                screenView.navigationController.push(AppScreenTypes.SavedScreen)
            }
        }

        onSettingsButtonClicked: {
            if (screenView.navigationController.contains(AppScreenTypes.SettingsScreen)) {
                screenView.navigationController.popToScreen(AppScreenTypes.SettingsScreen)
            } else {
                screenView.navigationController.push(AppScreenTypes.SettingsScreen)
            }
        }
    }
}
