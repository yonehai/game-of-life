#ifndef GAMEBOARD_H
#define GAMEBOARD_H

#include <QQuickPaintedItem>

class GameBoardData;

class GameBoard : public QQuickPaintedItem
{
    Q_OBJECT
    Q_PROPERTY(GameBoardData* gameBoardData
               READ gameBoardData
               WRITE setGameBoardData
               NOTIFY gameBoardDataChanged)
public:
    explicit GameBoard(QQuickItem *parent = nullptr);

    void paint(QPainter *painter) override;
    void mousePressEvent(QMouseEvent *event) override;

    void setGameBoardData(GameBoardData *newGameBoardData);
    GameBoardData *gameBoardData();

    static void initialize();

signals:
    void gameBoardDataChanged();

private:
    int mGridWidth;
    int mItemWidth;
    int mCellWidth;
    int mBoardWidth;
    int mBoardMargin;

    GameBoardData *mGameBoardData;
};

#endif // GAMEBOARD_H
