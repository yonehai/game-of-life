#include "guiutils.h"

#include <QGuiApplication>
#include <QFontDatabase>
#include <QQmlEngine>

void GuiUtils::registerFonts()
{
    QFontDatabase::addApplicationFont(":/fonts/Saira-Bold.ttf");
    QFontDatabase::addApplicationFont(":/fonts/Saira-SemiBold.ttf");
    int id = QFontDatabase::addApplicationFont(":/fonts/Saira-Regular.ttf");
    QStringList loadedFontFamilies = QFontDatabase::applicationFontFamilies(id);
    if (!loadedFontFamilies.empty())
    {
        QString font = loadedFontFamilies.at(0);
        QGuiApplication::setFont(QFont(font));
    }
}
