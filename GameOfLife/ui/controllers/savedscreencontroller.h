#ifndef SAVEDSCREENCONTROLLER_H
#define SAVEDSCREENCONTROLLER_H

#include "basescreencontroller.h"

class QAbstractItemModel;

class SavedBoardsManager;
class SavedBoardsModel;
class GameEngine;

class SavedScreenController : public BaseScreenController
{
    Q_OBJECT
public:
    explicit SavedScreenController(SavedBoardsManager &savedBoardsManager,
                                   GameEngine &gameEngine,
                                   QObject* parent = nullptr);
    ~SavedScreenController() override = default;

    QString ui() const override;

    Q_INVOKABLE QAbstractItemModel *savedBoardsModel() const;
    Q_INVOKABLE void deleteBoard(int id);
    Q_INVOKABLE void loadBoard(int id);

private:
    SavedBoardsManager &mSavedBoardsManager;
    GameEngine &mGameEngine;

    SavedBoardsModel *mSavedBoardsModel;
};

#endif // SAVEDSCREENCONTROLLER_H
