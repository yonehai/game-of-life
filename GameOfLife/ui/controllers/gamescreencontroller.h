#ifndef GAMESCREENCONTROLLER_H
#define GAMESCREENCONTROLLER_H

#include "basescreencontroller.h"

class SavedBoardsManager;
class SettingsManager;
class GameBoardData;
class GameEngine;

class GameScreenController : public BaseScreenController
{
    Q_OBJECT
    Q_PROPERTY(int stepsCount READ stepsCount NOTIFY stepsCountChanged)
    Q_PROPERTY(int highscore READ highscore NOTIFY highscoreChanged)
public:
    explicit GameScreenController(GameEngine &gameEngine, SavedBoardsManager &savedBoardsManager,
                                  QObject* parent = nullptr);
    ~GameScreenController() override = default;

    QString ui() const override;

    Q_INVOKABLE void startGame() const;
    Q_INVOKABLE void pauseGame() const;
    Q_INVOKABLE void resetGame() const;
    Q_INVOKABLE void saveBoard(const QString &boardName) const;

    Q_INVOKABLE GameBoardData *gameBoardData() const;
    Q_INVOKABLE int stepsCount() const;
    Q_INVOKABLE int highscore() const;

signals:
    void stepsCountChanged();
    void highscoreChanged();
    void gameOver(int stepsCount);

private:
    GameEngine &mGameEngine;
    SavedBoardsManager &mSavedBoardsManager;
};

#endif // GAMESCREENCONTROLLER_H
