#ifndef MENUSCREENCONTROLLER_H
#define MENUSCREENCONTROLLER_H

#include "basescreencontroller.h"

class MenuScreenController : public BaseScreenController
{
    Q_OBJECT
public:
    explicit MenuScreenController(QObject* parent = nullptr);
    ~MenuScreenController() override = default;

    QString ui() const override;

signals:
    void startButtonClicked();
    void savedButtonClicked();
    void settingsButtonClicked();
    void aboutButtonClicked();
};

#endif // MENUSCREENCONTROLLER_H
