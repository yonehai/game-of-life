#include "savedscreencontroller.h"

#include "navigationcontroller.h"

#include "../../core/savedboardsmanager.h"
#include "../../core/savedboardsmodel.h"
#include "../../core/gameengine.h"
#include "../appscreenfactory.h"

SavedScreenController::SavedScreenController(SavedBoardsManager &savedBoardsManager,
                                             GameEngine &gameEngine,
                                             QObject *parent)
    : BaseScreenController(parent)
    , mSavedBoardsManager(savedBoardsManager)
    , mGameEngine(gameEngine)
    , mSavedBoardsModel(new SavedBoardsModel(mSavedBoardsManager, this))
{
    mIsBackAvailable = true;
}

QString SavedScreenController::ui() const
{
    return QStringLiteral("qrc:/screens/SavedScreen.qml");
}

QAbstractItemModel *SavedScreenController::savedBoardsModel() const
{
    return mSavedBoardsModel;
}

void SavedScreenController::deleteBoard(int id)
{
    mSavedBoardsModel->deleteBoard(id);
}

void SavedScreenController::loadBoard(int id)
{
    mGameEngine.setGameBoardData(*mSavedBoardsManager.findBoard(id));
    navigationController().push(AppScreenTypes::GameScreen);
}
