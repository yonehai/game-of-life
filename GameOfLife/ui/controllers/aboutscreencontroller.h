#ifndef ABOUTSCREENCONTROLLER_H
#define ABOUTSCREENCONTROLLER_H

#include "basescreencontroller.h"

class AboutScreenController : public BaseScreenController
{
    Q_OBJECT
public:
    explicit AboutScreenController(QObject* parent = nullptr);
    ~AboutScreenController() override = default;

    QString ui() const override;

    Q_INVOKABLE QString aboutGameText() const;

private:
    QString readAboutGameText();

    const QString mAboutGameTextFilePath;
    const QString mAboutGameText;
};

#endif // ABOUTSCREENCONTROLLER_H
