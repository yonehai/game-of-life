#include "settingsscreencontroller.h"

#include "../../core/settingsmanager.h"

SettingsScreenController::SettingsScreenController(SettingsManager &settingsManager,
                                                   QObject *parent)
    : BaseScreenController(parent)
    , mSettingsManager(settingsManager)
{
    connect(&mSettingsManager,
            &SettingsManager::boardSizeChanged,
            this,
            &SettingsScreenController::boardSizeChanged,
            Qt::UniqueConnection);
    connect(&mSettingsManager,
            &SettingsManager::stepDurationChanged,
            this,
            &SettingsScreenController::stepDurationChanged,
            Qt::UniqueConnection);
    connect(&mSettingsManager,
            &SettingsManager::stepsForSuccessTimeoutChanged,
            this,
            &SettingsScreenController::stepsForSuccessTimeoutChanged,
            Qt::UniqueConnection);

    mIsBackAvailable = true;
}

QString SettingsScreenController::ui() const
{
    return QStringLiteral("qrc:/screens/SettingsScreen.qml");
}

int SettingsScreenController::boardSize() const
{
    return mSettingsManager.boardSize();
}

void SettingsScreenController::setBoardSize(int newBoardSize)
{
    mSettingsManager.setBoardSize(newBoardSize);
}

int SettingsScreenController::stepDuration() const
{
    return mSettingsManager.stepDuration();
}

void SettingsScreenController::setStepDuration(int newStepDuration)
{
    mSettingsManager.setStepDuration(newStepDuration);
}

int SettingsScreenController::stepsForSuccessTimeout() const
{
    return mSettingsManager.stepsForSuccessTimeout();
}

void SettingsScreenController::setStepsForSuccessTimeout(int newStepsForSuccessTimeout)
{
    mSettingsManager.setStepsForSuccessTimeout(newStepsForSuccessTimeout);
}
