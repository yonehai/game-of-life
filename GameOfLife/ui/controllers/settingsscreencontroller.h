#ifndef SETTINGSSCREENCONTROLLER_H
#define SETTINGSSCREENCONTROLLER_H

#include "basescreencontroller.h"

class SettingsManager;

class SettingsScreenController : public BaseScreenController
{
    Q_OBJECT
    Q_PROPERTY(int boardSize READ boardSize WRITE setBoardSize NOTIFY boardSizeChanged)
    Q_PROPERTY(int stepDuration READ stepDuration WRITE setStepDuration NOTIFY stepDurationChanged)
    Q_PROPERTY(int stepsForSuccessTimeout READ stepsForSuccessTimeout
               WRITE setStepsForSuccessTimeout NOTIFY stepsForSuccessTimeoutChanged)
public:
    explicit SettingsScreenController(SettingsManager &settingsManager, QObject* parent = nullptr);
    ~SettingsScreenController() override = default;

    QString ui() const override;

    int boardSize() const;
    void setBoardSize(int newBoardSize);

    int stepDuration() const;
    void setStepDuration(int newStepDuration);

    int stepsForSuccessTimeout() const;
    void setStepsForSuccessTimeout(int newStepsForSuccessTimeout);

signals:
    void boardSizeChanged(int boardSize);
    void stepDurationChanged(int stepDuration);
    void stepsForSuccessTimeoutChanged(int stepsForSuccessTimeout);

private:
    SettingsManager &mSettingsManager;
};

#endif // SETTINGSSCREENCONTROLLER_H
