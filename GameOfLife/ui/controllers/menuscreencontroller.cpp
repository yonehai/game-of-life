#include "menuscreencontroller.h"
#include "../appscreenfactory.h"

#include "navigationcontroller.h"

MenuScreenController::MenuScreenController(QObject *parent) : BaseScreenController(parent)
{
    connect(this,
            &MenuScreenController::aboutButtonClicked,
            this,
            [this]()
    {
        navigationController().push(AppScreenTypes::AboutScreen, QVariant(), 0);
    });
    connect(this,
            &MenuScreenController::startButtonClicked,
            this,
            [this]()
    {
        navigationController().push(AppScreenTypes::GameScreen, QVariant(), 0);
    });
    connect(this,
            &MenuScreenController::savedButtonClicked,
            this,
            [this]()
    {
        navigationController().push(AppScreenTypes::SavedScreen, QVariant(), 0);
    });
    connect(this,
            &MenuScreenController::settingsButtonClicked,
            this,
            [this]()
    {
        navigationController().push(AppScreenTypes::SettingsScreen, QVariant(), 0);
    });

    mIsBackAvailable = false;
}

QString MenuScreenController::ui() const
{
    return QStringLiteral("qrc:/screens/MenuScreen.qml");
}
