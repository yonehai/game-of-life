#include "aboutscreencontroller.h"

#include <QFile>

AboutScreenController::AboutScreenController(QObject *parent)
    : BaseScreenController(parent)
    , mAboutGameTextFilePath(QStringLiteral(":/text/AboutGameOfLife.txt"))
    , mAboutGameText(readAboutGameText())
{
    mIsBackAvailable = true;
}

QString AboutScreenController::ui() const
{
    return QStringLiteral("qrc:/screens/AboutScreen.qml");
}

QString AboutScreenController::aboutGameText() const
{
    return mAboutGameText;
}

QString AboutScreenController::readAboutGameText()
{
    QFile file(mAboutGameTextFilePath);
    if(!file.open(QIODevice::ReadOnly))
    {
        qDebug("Failed to open %s file", qPrintable(mAboutGameTextFilePath));
        return QString();
    }
    else
    {
        return file.readAll();
    }
    file.close();
}
