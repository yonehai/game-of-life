#include "gamescreencontroller.h"

#include "../../core/savedboardsmanager.h"
#include "../../core/settingsmanager.h"
#include "../../core/gameboarddata.h"
#include "../../core/gameengine.h"

GameScreenController::GameScreenController(GameEngine &gameEngine,
                                           SavedBoardsManager &savedBoardsManager,
                                           QObject *parent)
    : BaseScreenController(parent)
    , mGameEngine(gameEngine)
    , mSavedBoardsManager(savedBoardsManager)
{
    connect(&mGameEngine,
            &GameEngine::highscoreChanged,
            this,
            &GameScreenController::highscoreChanged,
            Qt::UniqueConnection);
    connect(&mGameEngine,
            &GameEngine::stepsCountChanged,
            this,
            &GameScreenController::stepsCountChanged,
            Qt::UniqueConnection);
    connect(&mGameEngine,
            &GameEngine::gameOver,
            this,
            &GameScreenController::gameOver,
            Qt::UniqueConnection);

    mIsBackAvailable = true;
}

QString GameScreenController::ui() const
{
    return QStringLiteral("qrc:/screens/GameScreen.qml");
}

void GameScreenController::startGame() const
{
    mGameEngine.start();
}

void GameScreenController::pauseGame() const
{
    mGameEngine.pause();
}

void GameScreenController::resetGame() const
{
    mGameEngine.reset();
}

void GameScreenController::saveBoard(const QString &boardName) const
{
    mGameEngine.gameBoardData()->setBoardName(boardName);
    mSavedBoardsManager.addBoard(mGameEngine.gameBoardData());
}

GameBoardData *GameScreenController::gameBoardData() const
{
    return mGameEngine.gameBoardData();
}

int GameScreenController::stepsCount() const
{
    return mGameEngine.stepsCount();
}

int GameScreenController::highscore() const
{
    return mGameEngine.highscore();
}
