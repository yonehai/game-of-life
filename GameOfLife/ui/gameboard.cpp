#include "gameboard.h"

#include <QPainter>

#include "../core/gameboarddata.h"

GameBoard::GameBoard(QQuickItem *parent)
    : QQuickPaintedItem(parent)
    , mGridWidth(1)
    , mItemWidth(0)
    , mCellWidth(0)
    , mBoardWidth(0)
    , mBoardMargin(0)
    , mGameBoardData(nullptr)
{
    setAcceptedMouseButtons(Qt::AllButtons);
    setAntialiasing(true);
}

void GameBoard::paint(QPainter *painter)
{
    mItemWidth = size().width();
    mCellWidth = (mItemWidth - mGridWidth * (mGameBoardData->rowCount() + 1))
                 / mGameBoardData->rowCount();
    mBoardWidth = mCellWidth * mGameBoardData->rowCount() + mGridWidth
                  * (mGameBoardData->rowCount() + 1);
    mBoardMargin = (mItemWidth - mBoardWidth) / 2;

    QPen pen(QColor(116, 161, 121));
    pen.setWidth(mGridWidth);
    painter->setPen(pen);

    // INFO: Draw horisontal lines
    int lineY = mBoardMargin;
    for (int i = 0; i <= mGameBoardData->rowCount(); ++i)
    {
        painter->drawLine(mBoardMargin, lineY, mBoardMargin + mBoardWidth - mGridWidth, lineY);
        lineY += mCellWidth + mGridWidth;
    }

    // INFO: Draw vertical lines
    int lineX = mBoardMargin;
    for (int i = 0; i <= mGameBoardData->rowCount(); ++i)
    {
        painter->drawLine(lineX, mBoardMargin, lineX, mBoardMargin + mBoardWidth - mGridWidth);
        lineX += mCellWidth + mGridWidth;
    }

    QBrush brush(QColor(116, 161, 121));

    // INFO: Drawing alive cells
    int cellY = mBoardMargin + mGridWidth;
    for (const QVector<bool> &cellsRow : qAsConst(mGameBoardData->cells()))
    {
        int cellX = mBoardMargin + mGridWidth;
        for (bool cell : cellsRow)
        {
            if (cell == true)
            {
                QRectF cell(cellX, cellY, mCellWidth - 1, mCellWidth - 1);
                painter->fillRect(cell, brush);
            }
            cellX += mCellWidth + mGridWidth;
        }
        cellY += mCellWidth + mGridWidth;
    }
}

void GameBoard::mousePressEvent(QMouseEvent *event)
{
    int xOffset = (event->x() - mBoardMargin) / (mCellWidth + mGridWidth);
    int yOffset = (event->y() - mBoardMargin) / (mCellWidth + mGridWidth);
    if ((xOffset < mGameBoardData->rowCount()) && (yOffset < mGameBoardData->rowCount()))
    {
        mGameBoardData->cells()[yOffset][xOffset] = !mGameBoardData->cells()[yOffset][xOffset];
        update();
        emit mGameBoardData->isEmptyChanged();
    }
}

void GameBoard::setGameBoardData(GameBoardData *newGameBoardData)
{
    if (mGameBoardData != newGameBoardData)
    {
        mGameBoardData = newGameBoardData;
        connect(mGameBoardData, &GameBoardData::updated, this, [this] () {
            update();
        });
        emit gameBoardDataChanged();
    }
}

GameBoardData *GameBoard::gameBoardData()
{
    return mGameBoardData;
}

void GameBoard::initialize()
{
    qmlRegisterType<GameBoard>("GameBoard", 1, 0, "GameBoard");
}
