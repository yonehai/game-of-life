#include "appscreenfactory.h"

#include <QQmlEngine>

#include "basescreencontroller.h"

#include "controllers/settingsscreencontroller.h"
#include "controllers/aboutscreencontroller.h"
#include "controllers/savedscreencontroller.h"
#include "controllers/menuscreencontroller.h"
#include "controllers/gamescreencontroller.h"
#include "../core/appmanager.h"

AppScreenFactory::AppScreenFactory(AppManager &appManager)
    : AbstractScreenFactory()
    , mAppManager(appManager)
{
}

std::unique_ptr<BaseScreenController> AppScreenFactory::makeScreen(int screenType,
                                                                   const QVariant &data)
{
    std::unique_ptr<BaseScreenController> newController;
    switch (screenType)
    {
    case AppScreenTypes::MenuScreen:
        newController = std::make_unique<MenuScreenController>();
        break;
    case AppScreenTypes::AboutScreen:
        newController = std::make_unique<AboutScreenController>();
        break;
    case AppScreenTypes::GameScreen:
        newController = std::make_unique<GameScreenController>(mAppManager.gameEngine(),
                                                               mAppManager.savedBoardsManager());
        break;
    case AppScreenTypes::SavedScreen:
        newController = std::make_unique<SavedScreenController>(mAppManager.savedBoardsManager(),
                                                                mAppManager.gameEngine());
        break;
    case AppScreenTypes::SettingsScreen:
        newController = std::make_unique<SettingsScreenController>(mAppManager.settingsManager());
        break;
    }
    if (newController)
    {
        newController->init(data);
        newController->setScreenType(screenType);
    }
    return newController;
}

void AppScreenFactory::initialize()
{
    qmlRegisterUncreatableType<AppScreenTypes>(
                "AppScreenTypes",
                1,
                0,
                "AppScreenTypes",
                QStringLiteral("Could not instantiate AppScreenTypes enum type."));
}
