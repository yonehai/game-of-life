#ifndef APPSCREENFACTORY_H
#define APPSCREENFACTORY_H

#include "abstractscreenfactory.h"
#include "controllertypes.h"

class AppManager;

class AppScreenTypes : public ControllerTypes
{
    Q_GADGET
public:
    enum Types
    {
        MenuScreen = ControllerTypes::UserController + 1,
        GameScreen,
        SavedScreen,
        SettingsScreen,
        AboutScreen
    };
    Q_ENUM(Types)
};

class AppScreenFactory : public AbstractScreenFactory
{
public:
    explicit AppScreenFactory(AppManager &appManager);

    std::unique_ptr<BaseScreenController> makeScreen(int screenType,
                                                     const QVariant& data = QVariant()) override;
    void initialize() override;

private:
    AppManager &mAppManager;
};

#endif // APPSCREENFACTORY_H
